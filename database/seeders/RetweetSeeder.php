<?php

namespace Database\Seeders;

use App\Models\Retweet;
use Illuminate\Database\Seeder;

class RetweetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Retweet::factory()->create();
        }
    }
}
