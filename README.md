<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Project
Own version of Twitter

## Please create a readme with some thoughts about the implementation you have done

- I tried to fulfill all tasks, so I didn't look up for details like successful message or error messages. 
- I didn't waste time for complex validation rules, so security is on low level.
- I didn't authorize every route. 
- And much more. 

- My idea was to show you on small examples that I know how to use tools and instead to do repetitive work to be "perfect",
- I just skipped some things because I showed that I know that on different examples.
- And I supposed that we can talk about those things in live coding session.

- I tried to follow KISS principle, I hope that I manage to succeed in that :) :D. 

- I didn't move CRUD operations from controllers because task has simple operation, but I really prefer to have "fat models"
- and "tiny controllers".

- I would love to write some test but there was no time for that. 

- I hope that I answered as you expected.

Good night, have I nice weekend :)

