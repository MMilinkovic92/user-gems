<?php

namespace Tests\Feature;

use App\Models\Tweet;
use App\Models\User;
use App\Notifications\CensoredTextNotification;
use App\Services\CleanTweet\DataTransferObjects\CleanTextDto;
use App\Services\CleanTweet\Services\PurgoLanguageService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Mockery\MockInterface;
use Tests\TestCase;

class CleanTweetTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     *
     * Create Clean Tweet without obscene words.
     *
     * @return void
     */
    public function successfully_create_clean_tweet()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $obsceneWord = "jackass";
        $obsceneWordReplace = "*******";
        $obsceneSentence = "This is test for obscene words: %s";
        $title = "Test obscene word";

        $dto = new CleanTextDto([
            'originalText' => sprintf($obsceneSentence, $obsceneWord),
            'cleanText' => sprintf($obsceneSentence, $obsceneWordReplace),
            'replacedCharCount' => strlen($obsceneWord)
        ]);

        $this->partialMock(PurgoLanguageService::class, function (MockInterface $mock) use ($dto) {
            $mock->shouldReceive('execute')->andReturn($dto)->once();
        });

        Notification::fake();

        $this->post(route('clean-tweets.store'), [
            'title' => $title,
            'description' => sprintf($obsceneSentence, $obsceneWord),
        ]);

        Notification::assertSentTo(
            [$user], CensoredTextNotification::class
        );

        $this->assertDatabaseHas('tweets', [
            'title' => $title,
            'description' => sprintf($obsceneSentence, $obsceneWordReplace)
        ]);
    }
}
