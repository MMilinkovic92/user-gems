<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    #region Relationships
    /**
     * Return all tweets related to this User.
     *
     * @return HasMany
     */
    public function tweets(): HasMany
    {
        return $this->hasMany(Tweet::class);
    }

    /**
     * Return all users that follows this object.
     *
     * @return BelongsToMany
     */
    public function followers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'follows', 'leader_id', 'follower_id');
    }

    /**
     * Return all users that this object following.
     *
     * @return BelongsToMany
     */
    public function following(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'follows', 'follower_id', 'leader_id');
    }

    /**
     * Return retweeted tweets.
     *
     * @return BelongsToMany
     */
    public function retweets(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'retweets', 'user_id', 'tweet_id');
    }
    #endregion

    #region Scopes

    /**
     * Find all users that current authenticated user doesn't follow.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeNotFollowedByCurrentUser(Builder $query): Builder
    {
        return $query->whereNotIn('id', Auth::user()->following->pluck('id'));
    }
    #endregion

    #region Methods
    /**
     * Check user role.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->is_admin;
    }
    #endregion

    #region Queries
    /**
     * Return user ids ordered by all followers. So, the first one is leader with most influence.
     *
     * @return array
     */
    public function getLeadersOrderedByInfluence(): array
    {
        $query = "
        SELECT u.id, (SELECT COUNT(f2.follower_id) FROM follows f2 WHERE f2.leader_id = u.id) AS followers
        FROM users AS u
        WHERE u.id IN (SELECT u2.id
				FROM users AS u2
				INNER JOIN follows AS f ON f.leader_id = u2.id
				WHERE f.follower_id = ?
			  )
        ORDER BY followers DESC
        ";

        return DB::select(DB::raw($query), [$this->id]);
    }
    #endregion
}
