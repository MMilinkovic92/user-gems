<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Tweet extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'description',
        'user_id',
    ];

    #region Relationships
    /**
     * Get tweet author.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    #endregion

    #region Scopes

    /**
     * We need to order by tweets by most influence leader.
     *
     * @param Builder $query
     * @param User $user
     * @return Builder
     */
    public function scopeOrderByLeaderInfluence(Builder $query, User $user): Builder
    {
        $getUsersWithMostInfluence = $user->getLeadersOrderedByInfluence();

        return $query
                ->whereIn('user_id', array_column($getUsersWithMostInfluence, 'id'))
                ->orderByRaw("FIELD(user_id, ". implode(',', array_column($getUsersWithMostInfluence, 'id')) .")");
    }

    /**
     * Return newest tweets. Newest tweets are tweets created day before and newer.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeNewest(Builder $query): Builder
    {
        return $query->where('created_at', '>=', Carbon::now()->subDay()->toDateString());
    }
    #endregion
}
