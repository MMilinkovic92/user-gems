<?php

namespace App\Services\CleanTweet\Services;

use App\Services\CleanTweet\Contracts\CleanLanguageServiceInterface;
use App\Services\CleanTweet\DataTransferObjects\CleanTextDto;
use Illuminate\Support\Facades\Http;

class PurgoLanguageService implements CleanLanguageServiceInterface
{
    protected const PROTOCOL = "https://";

    protected array $requestData;

    /**
     * Execute check for censored words.
     *
     * @return CleanTextDto
     */
    public function execute(): CleanTextDto
    {
        $response = Http::get($this->generateUrl());
        $censured = $response->json()['result'];
        $replacedCharsCount = substr_count($censured, '*');

        return new CleanTextDto([
            'originalText' => $this->requestData['text'],
            'cleanText' => $censured,
            'replacedCharCount' => $replacedCharsCount
        ]);
    }

    /**
     * Generate valid URL for third party service.
     * This should go to helper.
     *
     * @return string
     */
    private function generateUrl()
    {
        return self::PROTOCOL . config('clean-language.clean_tweet_service') . '?' . http_build_query($this->requestData);
    }

    /**
     * Set data for request.
     *
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->requestData = $data;
        return $this;
    }
}
