<?php

namespace App\Services\CleanTweet\DataTransferObjects;

class CleanTextDto
{
    public string $originalText;
    public string $censuredText;
    public int $replacedCharsCount;

    public function __construct(array $data)
    {
        $this->originalText = $data['originalText'];
        $this->censuredText = $data['cleanText'];
        $this->replacedCharsCount = $data['replacedCharCount'];
    }

    public function toArray()
    {
        return [
            'originalText' => $this->originalText,
            'censuredText' => $this->censuredText,
            'replacedCharsCount' => $this->replacedCharsCount
        ];
    }
}
