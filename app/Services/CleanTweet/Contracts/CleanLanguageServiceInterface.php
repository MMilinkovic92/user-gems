<?php

namespace App\Services\CleanTweet\Contracts;

use App\Services\CleanTweet\DataTransferObjects\CleanTextDto;

interface CleanLanguageServiceInterface
{
    public function execute(): CleanTextDto;
}
