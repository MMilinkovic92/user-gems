<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCleanTweetRequest;
use App\Models\Tweet;
use App\Notifications\CensoredTextNotification;
use App\Services\CleanTweet\Contracts\CleanLanguageServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CleanTweetController extends Controller
{
    protected CleanLanguageServiceInterface $cleanLanguageService;

    public function __construct(CleanLanguageServiceInterface $cleanLanguageService)
    {
        $this->cleanLanguageService = $cleanLanguageService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('clean-tweets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCleanTweetRequest $request
     * @return RedirectResponse
     */
    public function store(StoreCleanTweetRequest $request)
    {
        $response = $this->cleanLanguageService->setData(['text' => $request->description])->execute();

        $tweet = Tweet::create([
            'title' => $request->title,
            'description' => $response->censuredText,
            'user_id' => Auth::user()->id
        ]);

        Auth::user()->notify(new CensoredTextNotification($tweet, $response->toArray()));

        return redirect()->route('tweets.index');
    }
}
