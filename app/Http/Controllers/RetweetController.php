<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRetweetRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class RetweetController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRetweetRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRetweetRequest $request): RedirectResponse
    {
        Auth::user()->retweets()->attach($request->get('tweet_id'));

        return redirect()->back()->with(['message' => Lang::get('Successfully retweeted')]);
    }
}
