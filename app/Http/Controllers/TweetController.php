<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTweetRequest;
use App\Models\Tweet;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $tweets = Tweet::with('user')
                        ->newest()
                        ->orderByLeaderInfluence(Auth::user())
                        ->paginate(5);

        return view('tweets.index')->with(['tweets' => $tweets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('tweets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTweetRequest $request
     * @return RedirectResponse
     */
    public function store(StoreTweetRequest $request): RedirectResponse
    {
        $request->merge(['user_id' => $request->user()->id]);

        Tweet::create($request->only(['title', 'description', 'user_id']));

        return redirect()->route('users.show', ['user' => Auth::user()]);
    }
}
