<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFakeTweetRequest;
use App\Models\Tweet;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class FakeTweetController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $users = User::where('id', '!=', Auth::user()->id)->get();

        return view('fake-tweets.create')->with(['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreFakeTweetRequest $request
     * @return RedirectResponse
     */
    public function store(StoreFakeTweetRequest $request): RedirectResponse
    {
        $user = User::where('username', $request->username)->first();

        Tweet::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $user->id
        ]);

        return redirect()->route('tweets.index');
    }
}
