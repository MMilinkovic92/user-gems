<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFollowRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\View;

class FollowController extends Controller
{
    /**
     * Display a listing of people that current user follows.
     *
     * @return View
     */
    public function index(): View
    {
        $followedByUser = Auth::user()->following()->paginate(5);

        return view('follows.index')->with(['followedByUser' => $followedByUser]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFollowRequest $request
     * @return RedirectResponse
     */
    public function store(StoreFollowRequest $request): RedirectResponse
    {
        Auth::user()->following()->attach($request->only(['leader_id']));

        return redirect()->back()->with(['message' => Lang::get('Successfully followed')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $follow
     * @return RedirectResponse
     */
    public function destroy(int $follow): RedirectResponse
    {
        Auth::user()->following()->detach($follow);

        return redirect()->back()->with(['message' => Lang::get('Successfully unfollowed')]);
    }
}
