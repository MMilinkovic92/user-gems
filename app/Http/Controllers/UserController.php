<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $users = User::notFollowedByCurrentUser()->paginate(5);

        return view('users.index')->with(['users' => $users]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id): View
    {
        $tweets = User::find($id)->tweets()->orderBy('created_at', 'desc')->paginate(5);

        return view('users.show')->with(['tweets' => $tweets]);
    }
}
