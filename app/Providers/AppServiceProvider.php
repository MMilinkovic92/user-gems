<?php

namespace App\Providers;

use App\Services\CleanLanguageService;
use App\Services\CleanTweet\Contracts\CleanLanguageServiceInterface;
use App\Services\CleanTweet\Services\PurgoLanguageService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        CleanLanguageServiceInterface::class => PurgoLanguageService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
