<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tweeter feed') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                @foreach($tweets AS $tweet)
                    <div class="p-6 bg-white border-b border-gray-200">

                        <div class="p-6 bg-white border-b border-gray-200">
                            {{__('Title: ')}} {{$tweet->title}}  <br>

                            {{__('Description: ')}} {{$tweet->description}} <br>

                            {{__('Author: ')}} {{$tweet->user->username}}
                        </div>

                        <form method="POST" action="{{ route('retweets.store') }}">
                            @csrf

                            <input type="hidden" value="{{$tweet->user->id}}" name="user_id" id="user_id">
                            <input type="hidden" value="{{$tweet->id}}" name="tweet_id" id="tweet_id">

                            <x-button class="ml-3">
                                {{ __('Retweet') }}
                            </x-button>
                        </form>

                    </div>
                @endforeach

            </div>

            {{ $tweets->links() }}
        </div>
    </div>
</x-app-layout>
