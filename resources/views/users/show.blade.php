<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Show my tweets') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                    @foreach($tweets AS $tweet)
                        <div class="p-6 bg-white border-b border-gray-200">
                                {{$tweet->title}}  </br>

                                {{$tweet->description}} </br>
                        </div>
                    @endforeach

            </div>

            {{ $tweets->links() }}
        </div>
    </div>
</x-app-layout>
