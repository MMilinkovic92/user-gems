<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Show all users that I follow') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                @foreach($followedByUser AS $user)
                    <div class="p-6 bg-white border-b border-gray-200">
                        {{$user->name}}  </br>

                        {{$user->email}}

                        <form method="POST" action="{{ route('follows.destroy', ['follow' => $user->id]) }}">
                            @csrf
                            @method('DELETE')
                            <x-button class="ml-3">
                                {{ __('Unfollow') }}
                            </x-button>
                        </form>
                    </div>
                @endforeach

            </div>

            {{ $followedByUser->links() }}
        </div>
    </div>
</x-app-layout>
