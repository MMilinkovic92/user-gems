<?php

use App\Http\Controllers\CleanTweetController;
use App\Http\Controllers\FakeTweetController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\RetweetController;
use App\Http\Controllers\TweetController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('users', UserController::class)->only(['index', 'show']);
Route::resource('tweets', TweetController::class)->only(['index', 'create', 'store']);
Route::resource('follows', FollowController::class)->only(['index', 'store', 'destroy']);
Route::resource('retweets', RetweetController::class)->only(['store']);
Route::resource('fake-tweets', FakeTweetController::class)->middleware('admin')->only(['create', 'store']);
Route::resource('clean-tweets', CleanTweetController::class)->only(['create', 'store']);

require __DIR__.'/auth.php';
